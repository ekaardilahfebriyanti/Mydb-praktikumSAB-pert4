package com.ekaardilahfebriyanti.myui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ButtonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);
    }

    public void toBlack(View view) {
        //Now get handle to any view contained
        // within the main layout you are using
        View someView = findViewById(R.id.bgColorToBlack);
        // Find the root view
        View root = someView.getRootView();
        // Set the color
        root.setBackgroundColor(getResources().getColor(android.R.color.black));
    }

    public void toBlue(View view) {
        //Now get handle to any view contained
        // within the main layout you are using
        View someView = findViewById(R.id.bgColorToBlue);
        // Find the root view
        View root = someView.getRootView();
        // Set the color
        root.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
    }

    public void toRed(View view) {
        //Now get handle to any view contained
        // within the main layout you are using
        View someView = findViewById(R.id.bgColorToRed);
        // Find the root view
        View root = someView.getRootView();
        // Set the color
        root.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
    }
}
